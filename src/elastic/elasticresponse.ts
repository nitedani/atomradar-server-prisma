export interface ElasticResponse {
  meta: { request_id: string; page: { total_results: number } };
  results: {
    id: { raw: string };
    name: { raw: string };
    cover_image: { raw: string };
    _meta: { score: number };
  }[];
}
