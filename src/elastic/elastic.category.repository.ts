import { Injectable } from "@nestjs/common";
const AppSearchClient = require("@elastic/app-search-node");
import got from "got";
import { ClickThrough } from "../controller/app";
import { Category } from "@generated";
import { CategoryWithScore } from "../search/search";
import { ElasticResponse } from "./elasticresponse";
import { Projection } from "./projection";
import { Language } from "@prisma/client";

@Injectable()
export class ElasticCategoryRepository {
  static storedInElastic: Projection<CategoryWithScore> = [
    "name",
    "url_safe_name",
    "cover_image",
    "id",
    "score",
    "engine",
    "request_id",
  ];
  private static client = new AppSearchClient(
    undefined,
    process.env.ELASTIC_API_KEY,
    () =>
      `http://${process.env.ELASTIC_HOST}:${process.env.ELASTIC_PORT}/api/as/v1/`
  );
  public static engineName = "categories";

  static async index(categories: Category | Category[]): Promise<any> {
    if (!Array.isArray(categories)) categories = [categories];

    return this.client.indexDocuments(
      this.engineName,
      categories.map((category) => ({
        id: category.id,
        ...category.localNames?.reduce(
          (obj, localName) =>
            Object.assign(obj, {
              [`${localName.lang.toLowerCase()}_name`]: localName.value,
            }),
          {}
        ),
        cover_image: category.cover_image,
      }))
    );
  }

  static async click({ query, request_id, document_id, tags }: ClickThrough) {
    return got.post(
      `${process.env.ELASTIC_PROTOCOL}://${process.env.ELASTIC_HOST}:${process.env.ELASTIC_PORT}/api/as/v1/engines/${this.engineName}/click`,
      {
        json: {
          query,
          request_id,
          document_id,
          tags,
        },
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${process.env.ELASTIC_API_KEY}`,
        },
      }
    );
  }

  static async deleteMany(categories: Category[]): Promise<any> {
    this.client.updateDocuments(
      this.engineName,
      categories.map((category) => category.id)
    );
  }

  static async delete(category: Category): Promise<any> {
    this.client.updateDocuments(this.engineName, [category.id]);
  }

  static async update(category: Category): Promise<any> {
    this.client.updateDocuments(this.engineName, [
      {
        id: category.id,
        ...category.localNames?.reduce(
          (obj, localName) =>
            Object.assign(obj, {
              [`${localName.lang.toLowerCase()}_name`]: localName.value,
            }),
          {}
        ),
        cover_image: category.cover_image,
      },
    ]);
  }

  static async updateMany(categories: Category[]): Promise<any> {
    this.client.updateDocuments(
      this.engineName,
      categories.map((category) => ({
        id: category.id,
        ...category.localNames?.reduce(
          (obj, localName) =>
            Object.assign(obj, {
              [`${localName.lang.toLowerCase()}_name`]: localName.value,
            }),
          {}
        ),
        cover_image: category.cover_image,
      }))
    );
  }

  static async searchName(
    categoryName: string,
    lang: Language,
    limit = 10
  ): Promise<ElasticResponse> {
    const _lang = lang.toLowerCase();
    const response = await this.client.search(this.engineName, categoryName, {
      search_fields: { [`${_lang}_name`]: {} },
      result_fields: {
        id: { raw: {} },
        [`${_lang}_name`]: {
          raw: {},
        },
        cover_image: { raw: {} },
      },
      page: {
        size: limit,
        current: 1,
      },
    });

    response.results = response.results.map((result: any) => ({
      ...result,
      name: result[`${_lang}_name`],
    }));

    return response;
  }
}
