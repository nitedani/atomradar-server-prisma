import { MiddlewareFn, UseMiddleware } from "type-graphql";
import {
  Category,
  Product,
  ResolversEnhanceMap,
  applyResolversEnhanceMap,
} from "@generated";
import { ElasticCategoryRepository } from "./elastic.category.repository";
import { ElasticProductRepository } from "./elastic.product.repository";
import { Context } from "..";

export class ElasticMiddleware {
  public static applyResolversEnhanceMap() {
    const resolversEnhanceMap: ResolversEnhanceMap = {
      Product: {
        createProduct: [UseMiddleware(ElasticMiddleware.indexProduct)],
        updateProduct: [UseMiddleware(ElasticMiddleware.updateProduct)],
        deleteProduct: [UseMiddleware(ElasticMiddleware.deleteProduct)],
        updateManyProduct: [UseMiddleware(ElasticMiddleware.updateManyProduct)],
        deleteManyProduct: [UseMiddleware(ElasticMiddleware.deleteManyProduct)],
        upsertProduct: [UseMiddleware(ElasticMiddleware.indexProduct)],
      },
      Category: {
        createCategory: [UseMiddleware(ElasticMiddleware.indexCategory)],
        updateCategory: [UseMiddleware(ElasticMiddleware.updateCategory)],
        deleteCategory: [UseMiddleware(ElasticMiddleware.deleteCategory)],
        updateManyCategory: [
          UseMiddleware(ElasticMiddleware.updateManyCategory),
        ],
        deleteManyCategory: [
          UseMiddleware(ElasticMiddleware.deleteManyCategory),
        ],
      },
    };
    applyResolversEnhanceMap(resolversEnhanceMap);
  }
  public static indexProduct: MiddlewareFn<Context> = async (
    { context: { prisma } },
    next
  ) => {
    let savedProduct: Product = await next();
    const saved = await prisma.product.findFirst({
      where: { id: savedProduct.id },
      select: { id: true, localNames: true, cover_image: true },
    });

    await ElasticProductRepository.index(saved!);
    return savedProduct;
  };

  public static updateProduct: MiddlewareFn<Context> = async (
    { context: { prisma } },
    next
  ) => {
    let savedProduct: Product = await next();
    const saved = await prisma.product.findFirst({
      where: { id: savedProduct.id },
      select: { id: true, localNames: true, cover_image: true },
    });

    await ElasticProductRepository.update(saved!);
    return savedProduct;
  };

  public static deleteProduct: MiddlewareFn = async ({ info }, next) => {
    const deltedProduct: Product = await next();
    await ElasticProductRepository.delete(deltedProduct);
    return deltedProduct;
  };

  public static updateManyProduct: MiddlewareFn = async ({ info }, next) => {
    return next();
  };

  public static deleteManyProduct: MiddlewareFn = async ({ info }, next) => {
    return next();
  };

  public static indexCategory: MiddlewareFn<Context> = async (
    { context: { prisma } },
    next
  ) => {
    let savedCategory: Category = await next();
    const saved = await prisma.category.findFirst({
      where: { id: savedCategory.id },
      select: { id: true, localNames: true, cover_image: true },
    });

    await ElasticCategoryRepository.index(saved!);
    return savedCategory;
  };

  public static updateCategory: MiddlewareFn<Context> = async (
    { context: { prisma } },
    next
  ) => {
    let savedCategory: Category = await next();
    const saved = await prisma.category.findFirst({
      where: { id: savedCategory.id },
      select: { id: true, localNames: true, cover_image: true },
    });

    await ElasticCategoryRepository.update(saved!);
    return savedCategory;
  };

  public static deleteCategory: MiddlewareFn = async ({ info }, next) => {
    const deltedCategory: Product = await next();
    await ElasticCategoryRepository.delete(deltedCategory);
    return deltedCategory;
  };

  public static updateManyCategory: MiddlewareFn = async ({ info }, next) => {
    return next();
  };

  public static deleteManyCategory: MiddlewareFn = async ({ info }, next) => {
    return next();
  };
}
