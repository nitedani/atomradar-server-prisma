import { ContextType } from "@nestjs/common";
import got from "got";

import { AuthChecker } from "type-graphql";

const authUrl = `${process.env.AUTH_PROTOCOL}://${process.env.AUTH_HOST}:${process.env.AUTH_PORT}`;

export const customAuthChecker: AuthChecker<ContextType> = async (
  { root, args, context, info },
  roles
) => {
  try {
    let authorization = await got(
      `${authUrl}/validatetoken?authority=${roles.join(",")}`,
      {
        headers: { Cookie: `jwt=${(context as any).req.cookies["jwt"]}` },
      }
    ).then((response) => JSON.parse(response.body));

    let user = await got(`${authUrl}/getuserinfo`, {
      method: "POST",
      responseType: "json",
      json: {
        email: authorization.email,
        requestedFields: ["id"],
      },
    }).then((response) => response.body);

    (context as any).user = user;
  } catch (error) {
    console.log(error);
    return false;
  }

  // here we can read the user from context
  // and check his permission in the db against the `roles` argument
  // that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]

  return true; // or false if access is denied
};
