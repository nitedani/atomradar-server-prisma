const AppSearchClient = require("@elastic/app-search-node");
import got from "got";
import { Product } from "@generated";
import { ClickThrough } from "../controller/app";
import { Projection } from "./projection";
import { ElasticResponse } from "./elasticresponse";
import { ProductWithScore } from "../search/search";
import { Language } from "@prisma/client";

export class ElasticProductRepository {
  static storedInElastic: Projection<ProductWithScore> = [
    "name",
    "url_safe_name",
    "cover_image",
    "id",
    "score",
    "engine",
    "request_id",
  ];
  private static client = new AppSearchClient(
    undefined,
    process.env.ELASTIC_API_KEY,
    () =>
      `http://${process.env.ELASTIC_HOST}:${process.env.ELASTIC_PORT}/api/as/v1/`
  );
  public static engineName = "products";

  static async index(products: Product | Product[]): Promise<any> {
    if (!Array.isArray(products)) products = [products];
    return this.client.indexDocuments(
      this.engineName,
      products.map((product) => ({
        id: product.id,
        ...product.localNames?.reduce(
          (obj, localName) =>
            Object.assign(obj, {
              [`${localName.lang.toLowerCase()}_name`]: localName.value,
            }),
          {}
        ),
        cover_image: product.cover_image,
      }))
    );
  }

  static async click({
    query,
    request_id,
    document_id,
    tags,
  }: ClickThrough): Promise<any> {
    return got.post(
      `${process.env.ELASTIC_PROTOCOL}://${process.env.ELASTIC_HOST}:${process.env.ELASTIC_PORT}/api/as/v1/engines/${this.engineName}/click`,
      {
        json: {
          query,
          request_id,
          document_id,
          tags,
        },
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${process.env.ELASTIC_API_KEY}`,
        },
      }
    );
  }

  static async updateMany(products: Product[]): Promise<any> {
    this.client.updateDocuments(
      this.engineName,
      products.map((product) => ({
        id: product.id,
        ...product.localNames?.reduce(
          (obj, localName) =>
            Object.assign(obj, {
              [`${localName.lang.toLowerCase()}_name`]: localName.value,
            }),
          {}
        ),
        cover_image: product.cover_image,
      }))
    );
  }

  static async deleteMany(products: Product[]): Promise<any> {
    this.client.updateDocuments(
      this.engineName,
      products.map((product) => product.id)
    );
  }

  static async delete(product: Product): Promise<any> {
    this.client.updateDocuments(this.engineName, [product.id]);
  }

  static async update(product: Product): Promise<any> {
    this.client.updateDocuments(this.engineName, [
      {
        ...product.localNames?.reduce(
          (obj, localName) =>
            Object.assign(obj, {
              [`${localName.lang.toLowerCase()}_name`]: localName.value,
            }),
          {}
        ),
        id: product.id,
        cover_image: product.cover_image,
      },
    ]);
  }

  static async addFields(
    product: Product,
    fields: Partial<Product>
  ): Promise<any> {
    this.client.updateDocuments(this.engineName, [
      { id: product.id, ...fields },
    ]);
  }

  static async searchName(
    productName: string,
    lang: Language,
    limit = 10
  ): Promise<ElasticResponse> {
    const _lang = lang.toLowerCase();
    const response = await this.client.search(this.engineName, productName, {
      search_fields: { [`${_lang}_name`]: {} },
      result_fields: {
        id: { raw: {} },
        [`${_lang}_name`]: {
          raw: {},
        },
        cover_image: { raw: {} },
      },
      page: {
        size: limit,
        current: 1,
      },
    });

    response.results = response.results.map((result: any) => ({
      ...result,
      name: result[`${_lang}_name`],
    }));

    return response;
  }
}
