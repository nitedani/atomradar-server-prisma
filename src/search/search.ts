import {
  Arg,
  Ctx,
  Field,
  FieldResolver,
  Float,
  Info,
  Int,
  ObjectType,
  Query,
  Resolver,
} from "type-graphql";
import { Context } from "..";
import { ElasticCategoryRepository } from "../elastic/elastic.category.repository";
import { ElasticProductRepository } from "../elastic/elastic.product.repository";
import { Projection } from "../elastic/projection";
import {
  Category,
  CategoryWhereInput,
  Product,
  ProductWhereInput,
} from "@generated";
import { unique } from "./shorthash";
import { CacheEmitter } from "./cacheemitter";
import { identity } from "rxjs";
import { Language } from "@prisma/client";
import { normalize } from "../nameToUrl/normalize";
@ObjectType()
export class ProductWithScore extends Product {
  @Field(() => String)
  name?: string;

  @Field(() => String)
  url_safe_name?: string;

  @Field(() => Float, { defaultValue: 0 })
  score?: number;

  @Field(() => String, { nullable: true })
  request_id?: string;

  @Field(() => String, { nullable: true })
  engine?: string;
}

@ObjectType()
export class CategoryWithScore extends Category {
  @Field(() => String)
  name?: string;

  @Field(() => String)
  url_safe_name?: string;

  @Field(() => Float, { defaultValue: 0 })
  score?: number;

  @Field(() => String, { nullable: true })
  request_id?: string;

  @Field(() => String, { nullable: true })
  engine?: string;
}
@ObjectType()
class ProductsWithCount {
  @Field(() => [ProductWithScore], { defaultValue: [] })
  results: ProductWithScore[] = [];

  @Field(() => Int, { defaultValue: 0 })
  count!: number;
}

@ObjectType()
class CategoriesWithCount {
  @Field(() => [CategoryWithScore], { defaultValue: [] })
  results: CategoryWithScore[] = [];

  @Field(() => Int, { defaultValue: 0 })
  count!: number;
}

@ObjectType()
class SearchResult {
  @Field(() => ProductsWithCount)
  products?: ProductsWithCount;

  @Field(() => CategoriesWithCount)
  categories?: CategoriesWithCount;
}

@Resolver(() => SearchResult)
export class SearchResolver {
  @FieldResolver(() => ProductsWithCount)
  async products(
    @Info() info: any,
    @Arg("take", () => Int, { nullable: true, defaultValue: 20 }) take: number,
    @Arg("skip", () => Int, { nullable: true, defaultValue: 0 }) skip: number,
    @Ctx()
    { prisma, extra }: Context,
    @Arg("where", { nullable: true, defaultValue: {} })
    where: ProductWhereInput,
    @Arg("term", () => String, { nullable: true })
    term: string,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<ProductsWithCount> {
    info.cacheControl.setCacheHint({ maxAge: 60 });
    const productProjection: Projection<Product> = info.fieldNodes[0].selectionSet.selections
      .find((selection: any) => selection.name.value === "results")
      .selectionSet.selections.map((selection: any) => selection.name.value)
      .filter(
        (item: string) =>
          ![
            "__typename",
            "score",
            "engine",
            "request_id",
            "cover_image",
            "url_safe_name",
            "name",
          ].includes(item)
      );
    const cacheKey =
      "products" +
      unique(JSON.stringify({ where, take, skip, productProjection }));
    extra.productsId?.put("id", cacheKey);

    const productsNeedHydrate = productProjection?.some(
      (key: keyof Product) =>
        !ElasticProductRepository.storedInElastic.includes(key)
    );

    if (term) {
      const elasticResponse = await ElasticProductRepository.searchName(
        term,
        lang,
        take
      );
      const elasticCount = elasticResponse.meta.page.total_results;
      let results: ProductWithScore[] = elasticResponse.results.map((hit) => ({
        id: hit.id.raw,
        name: hit.name.raw,
        url_safe_name: normalize(hit.name.raw),
        score: hit._meta.score,
        cover_image: hit.cover_image.raw,
        request_id: elasticResponse.meta.request_id,
        engine: "products",
      }));
      if (productsNeedHydrate) {
        const hitMap: { [key: string]: ProductWithScore } = {};

        where.id = {
          in: elasticResponse.results.map((hit) => hit.id.raw),
        };
        results.forEach((hit) => (hitMap[hit.id] = hit));
        results = await prisma.product
          .findMany({
            where,
            select: productProjection.reduce(
              (acc, curr) => ({ ...acc, [curr]: true }),
              { id: true }
            ),
            take,
            skip,
          })
          .then((products: Product[]) =>
            products.map((product) => ({
              ...product,
              ...hitMap[product.id],
            }))
          );
      }

      return { results, count: elasticCount };
    }
    const { count } = (await prisma.product.aggregate({ where })) as any;
    const results = await prisma.product.findMany({
      select: productProjection.reduce(
        (acc, curr) => ({ ...acc, [curr]: true }),
        { id: true, localNames: true }
      ),
      where,
      take,
      skip,
    });

    return {
      results: results.map((result) => {
        const name =
          result.localNames.find((localName) => localName.lang === lang)
            ?.value || result.localNames[0].value;
        return {
          ...result,
          name,
          url_safe_name: normalize(name),
        };
      }),
      count,
    };
  }

  @FieldResolver(() => CategoriesWithCount)
  async categories(
    @Arg("where", { nullable: true, defaultValue: {} })
    where: CategoryWhereInput,
    @Info() info: any,
    @Arg("take", () => Int, { nullable: true, defaultValue: 20 }) take: number,
    @Arg("skip", () => Int, { nullable: true, defaultValue: 0 }) skip: number,
    @Ctx() { prisma, extra }: Context,
    @Arg("term", () => String, { nullable: true })
    term: string,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<CategoriesWithCount> {
    info.cacheControl.setCacheHint({ maxAge: 60 });
    const categoryProjection: Projection<Product> = info.fieldNodes[0].selectionSet.selections
      .find((selection: any) => selection.name.value === "results")
      .selectionSet.selections.map((selection: any) => selection.name.value)
      .filter(
        (item: string) =>
          ![
            "__typename",
            "score",
            "engine",
            "request_id",
            "cover_image",
            "url_safe_name",
            "name",
          ].includes(item)
      );
    const cacheKey =
      "categories" +
      unique(JSON.stringify({ where, take, skip, categoryProjection }));
    extra.categoriesId?.put("id", cacheKey);

    const categoriesNeedHydrate = categoryProjection?.some(
      (key: keyof Product) =>
        !ElasticProductRepository.storedInElastic.includes(key)
    );

    if (term) {
      const elasticResponse = await ElasticCategoryRepository.searchName(
        term,
        lang,
        take
      );
      const elasticCount = elasticResponse.meta.page.total_results;

      let results: CategoryWithScore[] = elasticResponse.results.map((hit) => ({
        id: hit.id.raw,
        name: hit.name.raw,
        url_safe_name: normalize(hit.name.raw),
        score: hit._meta.score,
        cover_image: hit.cover_image.raw,
        request_id: elasticResponse.meta.request_id,
        engine: "categories",
      }));
      if (categoriesNeedHydrate) {
        const hitMap: { [key: string]: CategoryWithScore } = {};
        where.id = {
          in: elasticResponse.results.map((hit) => hit.id.raw),
        };
        results.forEach((hit) => (hitMap[hit.id] = hit));
        results = await prisma.category
          .findMany({
            where,
            select: categoryProjection.reduce(
              (acc, curr) => ({ ...acc, [curr]: true }),
              { id: true, name: true }
            ),
            take,
            skip,
          })
          .then((categories: Category[]) =>
            categories.map((category) => ({
              ...category,
              ...hitMap[category.id],
            }))
          );
      }

      return { results, count: elasticCount };
    }
    const { count } = (await prisma.category.aggregate({ where })) as any;
    const results = await prisma.category.findMany({
      select: categoryProjection.reduce(
        (acc, curr) => ({ ...acc, [curr]: true }),
        { id: true, localNames: true }
      ),
      where,
      take,
      skip,
    });
    return {
      results: results.map((result) => {
        const name =
          result.localNames.find((localName) => localName.lang === lang)
            ?.value || result.localNames[0].value;
        return {
          ...result,
          name,
          url_safe_name: normalize(name),
        };
      }),
      count,
    };
  }

  @FieldResolver(() => String, { defaultValue: "search" })
  async id(@Ctx() { extra }: Context, @Info() info: any) {
    info.cacheControl.setCacheHint({ maxAge: 60 });
    return Promise.all(
      [extra.productsId?.get("id"), extra.categoriesId?.get("id")].filter(
        identity
      )
    ).then((results: string[]) => "search" + results.join(""));
  }

  @Query(() => SearchResult)
  async search(@Info() info: any, @Ctx() ctx: Context) {
    info.cacheControl.setCacheHint({ maxAge: 60 });
    const fieldSelection = info.fieldNodes[0].selectionSet.selections.map(
      (selection: any) => selection.name.value
    );
    if (fieldSelection.includes("products")) {
      ctx.extra.productsId = new CacheEmitter();
    }

    if (fieldSelection.includes("categories")) {
      ctx.extra.categoriesId = new CacheEmitter();
    }
    return new SearchResult();
  }
}
