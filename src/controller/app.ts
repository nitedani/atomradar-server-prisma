import { Controller, Post, Get, Body, Param } from "@nestjs/common";
import { ElasticCategoryRepository } from "../elastic/elastic.category.repository";
import { ElasticProductRepository } from "../elastic/elastic.product.repository";

export interface ClickThrough {
  query: string;
  request_id: string;
  document_id: string;
  tags: string[];
}

@Controller("api")
export class AppController {
  @Post("click/:engine")
  click(@Body() click: ClickThrough, @Param("engine") engine: string): void {
    switch (engine) {
      case ElasticProductRepository.engineName:
        ElasticProductRepository.click(click);
        break;
      case ElasticCategoryRepository.engineName:
        ElasticCategoryRepository.click(click);
        break;
      default:
        break;
    }
    return;
  }

  @Get("health")
  health(): string {
    return "OK";
  }
}
