import { EventEmitter } from "events";

export class CacheEmitter extends EventEmitter {
  cache: { [key: string]: any };

  constructor() {
    super();
    this.cache = {};
  }

  get(key: string) {
    return new Promise((resolve, reject) => {
      // If value2 resolver already ran.
      if (this.cache[key]) {
        return resolve(this.cache[key]);
      }
      // If value2 resolver has not already run.
      this.on(key, (val) => {
        resolve(val);
      });
    });
  }

  put(key: string, value: any) {
    this.cache[key] = value;
    this.emit(key, value);
  }
}
