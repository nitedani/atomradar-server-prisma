import { PrismaClient } from "@prisma/client";
import { MiddlewareFn, UseMiddleware } from "type-graphql";
import {
  Category,
  CategoryAttribute,
  ResolversEnhanceMap,
  applyResolversEnhanceMap,
  AttributeDistinctFieldEnum,
} from "@generated";

export class CategoryTreeBuilder {
  public static applyResolversEnhanceMap() {
    const resolversEnhanceMap: ResolversEnhanceMap = {
      Product: {
        createProduct: [UseMiddleware(CategoryTreeBuilder.buildFilterList)],
        updateProduct: [UseMiddleware(CategoryTreeBuilder.buildFilterList)],
        deleteProduct: [UseMiddleware(CategoryTreeBuilder.buildFilterList)],
        updateManyProduct: [UseMiddleware(CategoryTreeBuilder.buildFilterList)],
        deleteManyProduct: [UseMiddleware(CategoryTreeBuilder.buildFilterList)],
        upsertProduct: [UseMiddleware(CategoryTreeBuilder.buildFilterList)],
      },
      Category: {
        createCategory: [
          UseMiddleware(
            CategoryTreeBuilder.buildCategoryTree,
            CategoryTreeBuilder.buildFilterList
          ),
        ],
        updateCategory: [
          UseMiddleware(
            CategoryTreeBuilder.buildCategoryTree,
            CategoryTreeBuilder.buildFilterList
          ),
        ],
        deleteCategory: [
          UseMiddleware(
            CategoryTreeBuilder.buildCategoryTree,
            CategoryTreeBuilder.buildFilterList
          ),
        ],
        updateManyCategory: [
          UseMiddleware(
            CategoryTreeBuilder.buildCategoryTree,
            CategoryTreeBuilder.buildFilterList
          ),
        ],
        deleteManyCategory: [
          UseMiddleware(
            CategoryTreeBuilder.buildCategoryTree,
            CategoryTreeBuilder.buildFilterList
          ),
        ],
      },
    };
    applyResolversEnhanceMap(resolversEnhanceMap);
  }
  static prisma = new PrismaClient();

  public static buildCategoryTree: MiddlewareFn = async ({ info }, next) => {
    const result = await next();
    const rootCategories = await CategoryTreeBuilder.prisma.category.findMany({
      where: { parentId: null },
      select: { localNames: true, id: true },
    });
    for (const c of rootCategories) {
      await CategoryTreeBuilder._buildCategoryTree(c);
    }
    return result;
  };

  public static buildFilterList: MiddlewareFn = async ({ info }, next) => {
    const result = await next();
    const rootCategories = await CategoryTreeBuilder.prisma.category.findMany({
      where: { parentId: null },
    });
    for (const c of rootCategories) {
      await CategoryTreeBuilder._buildCategoryProperties(c);
    }
    return result;
  };

  private static async _buildCategoryProperties(
    category: Category
  ): Promise<CategoryAttribute[]> {
    const childCategories = await CategoryTreeBuilder.prisma.category.findMany({
      where: { parentId: category.id },
    });

    if (!childCategories.length) {
      category.categoryAttributes = await CategoryTreeBuilder.prisma.categoryAttribute.findMany(
        {
          where: { categoryId: category.id },
        }
      );
      if (!category.categoryAttributes) return [];

      const filterListCategoryProps = category.categoryAttributes.filter(
        (property) => property.type == "FILTERLIST"
      );

      const props = await CategoryTreeBuilder.prisma.attribute.findMany({
        distinct: AttributeDistinctFieldEnum.value,
        where: {
          name: { in: filterListCategoryProps.map((p) => p.name) },
          product: {
            is: { categories: { some: { id: { equals: category.id } } } },
          },
        },
      });

      for (const listProp of filterListCategoryProps) {
        listProp.selectableValues = props
          .filter((prop) => prop.name == listProp.name)
          .map((prop) => prop.value);

        await CategoryTreeBuilder.prisma.categoryAttribute.update({
          where: { id: listProp.id },
          data: { selectableValues: listProp.selectableValues.sort() },
        });
      }

      return category.categoryAttributes;
    } else {
      let childCategoryProps: CategoryAttribute[][] = [];
      for (const child of childCategories) {
        childCategoryProps.push(
          await CategoryTreeBuilder._buildCategoryProperties(child)
        );
      }

      if (childCategoryProps.includes([])) {
        return [];
      }

      let commonCategoryProperties: CategoryAttribute[] = [];

      for (const name of childCategoryProps[0].map((p) => p.name)) {
        let common = true;
        const foundProps: CategoryAttribute[] = [];
        for (const categoryProps of childCategoryProps) {
          const foundProp = categoryProps.find((p) => p.name == name);
          if (!foundProp) {
            common = false;
            break;
          }
          foundProps.push(foundProp);
        }

        if (common) {
          foundProps.forEach((prop) => {
            prop.selectableValues?.forEach((value) => {
              if (!foundProps[0].selectableValues?.includes(value)) {
                foundProps[0].selectableValues?.push(value);
              }
            });
          });
          commonCategoryProperties.push(foundProps[0]);
        }
      }

      await CategoryTreeBuilder.prisma.categoryAttribute.deleteMany({
        where: { categoryId: category.id },
      });

      for (const categoryProp of commonCategoryProperties) {
        await CategoryTreeBuilder.prisma.category.update({
          where: { id: category.id },
          data: {
            categoryAttributes: {
              create: {
                name: categoryProp.name,
                type: categoryProp.type,
                selectableValues: categoryProp.selectableValues
                  ? categoryProp.selectableValues
                  : [],
              },
            },
          },
        });
      }

      return commonCategoryProperties;
    }
  }

  private static async _buildCategoryTree(
    parent: Category,
    path: Category[] = []
  ): Promise<void> {
    const _path = Array.from(path);
    _path.push(parent);
    const childCategories = await CategoryTreeBuilder.prisma.category.findMany({
      where: { parentId: parent.id },
      select: { localNames: true, id: true },
    });
    console.log(_path);

    if (!parent.parentId) {
      await CategoryTreeBuilder.prisma.category.update({
        where: { id: parent.id },
        data: {
          categoryTreeIds: {
            set: parent.id,
          },
        },
      });

      for (const name of parent.localNames!) {
        await CategoryTreeBuilder.prisma.categoryName.update({
          where: { id: name.id },
          data: {
            categoryTreeNames: {
              set: name.value,
            },
          },
        });
      }
    }

    if (!childCategories.length) {
      return;
    } else {
      for (const child of childCategories) {
        await CategoryTreeBuilder.prisma.category.update({
          where: { id: child.id },
          data: {
            categoryTreeIds: {
              set: [..._path.map((c) => c.id), child.id].join(">"),
            },
          },
        });

        for (const name of child.localNames) {
          await CategoryTreeBuilder.prisma.categoryName.update({
            where: { id: name.id },
            data: {
              categoryTreeNames: {
                set: [
                  ..._path.map(
                    (c) =>
                      c.localNames?.find((c_name) => c_name.lang === name.lang)
                        ?.value
                  ),
                  name.value,
                ].join(">"),
              },
            },
          });
        }

        await CategoryTreeBuilder._buildCategoryTree(child, _path);
      }
    }
  }
}
