import { MiddlewareFn, UseMiddleware } from "type-graphql";
import { ResolversEnhanceMap, applyResolversEnhanceMap } from "@generated";
import cacheManager from "cache-manager";
import { Projection } from "../elastic/projection";
import { Category } from "@prisma/client";
import { FieldNode } from "graphql";
import { unique } from "../search/shorthash";

export class CacheMiddleware {
  public static cacheManager = cacheManager.caching({
    store: "memory",
    ttl: 600,
  });

  public static applyResolversEnhanceMap() {
    const resolversEnhanceMap: ResolversEnhanceMap = {
      Product: { product: [UseMiddleware(CacheMiddleware.cacheById)] },
      Category: { category: [UseMiddleware(CacheMiddleware.cacheById)] },
    };
    applyResolversEnhanceMap(resolversEnhanceMap);
  }

  public static cacheById: MiddlewareFn = async ({ args, info }, next) => {
    const projection: Projection<Category> = info.fieldNodes[0]!.selectionSet!.selections.map(
      (selection: any) => selection.name.value
    ).filter(
      (item: string) =>
        !["__typename", "score", "engine", "request_id"].includes(item)
    );
    const id = args?.where?.id;
    const cacheKey = "categories" + unique(JSON.stringify({ id, projection }));
    console.log(cacheKey)

    if (cacheKey) {
      const cachedValue = await CacheMiddleware.cacheManager.get(cacheKey);
      if (cachedValue) {
        return cachedValue;
      }
    }
    const value = await next();
    await CacheMiddleware.cacheManager.set(cacheKey, value, { ttl: 600 });
    return value;
  };
}
