import "reflect-metadata";
import path from "path";
import {
  CategoryName,
  Description,
  Language,
  PrismaClient,
  ProductName,
} from "@prisma/client";
import {
  ResolversEnhanceMap,
  applyResolversEnhanceMap,
  crudResolvers,
  relationResolvers,
  Product,
  Category,
} from "@generated";
import {
  Arg,
  Authorized,
  Ctx,
  FieldResolver,
  registerEnumType,
  Resolver,
  Root,
  UseMiddleware,
} from "type-graphql";
import { TypeGraphQLModule } from "typegraphql-nestjs";
import { NestFactory } from "@nestjs/core";
import { CacheModule, Module, Provider } from "@nestjs/common";
import { ExpressAdapter } from "@nestjs/platform-express";
import cookieParser from "cookie-parser";
import { customAuthChecker } from "./auth/auth";
import { CategoryTreeBuilder } from "./categorytree/categorytree";
import { AppController } from "./controller/app";
import { SearchResolver } from "./search/search";
import { CacheMiddleware } from "./cache/cache";
import { ElasticMiddleware } from "./elastic/middleware";
import { normalize } from "./nameToUrl/normalize";

const responseCachePlugin = require("apollo-server-plugin-response-cache");

export interface Context {
  prisma: PrismaClient;
  req: Express.Request;
  res: Express.Response;
  extra: { [key: string]: any };
}

const resolversEnhanceMap: ResolversEnhanceMap = {
  User: {
    users: [Authorized(["ADMIN"])],
  },
};

@Resolver((of) => Product)
export class CustomProductResolver {
  @FieldResolver((type) => String, { nullable: true })
  async name(
    @Root() product: Product,
    @Ctx() { prisma, extra }: Context,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<string> {
    extra.productLocalNames ||= prisma.productName.findMany({
      where: { productId: product.id },
    });
    const localNames = await extra.productLocalNames;
    const foundName = localNames.find(
      (localName: ProductName) => localName.lang === lang
    )?.value;
    return foundName || localNames[0].value;
  }

  @FieldResolver((type) => String, { nullable: true })
  async url_safe_name(
    @Root() product: Product,
    @Ctx() { prisma, extra }: Context,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<string | undefined | null> {
    extra.productLocalNames ||= prisma.productName.findMany({
      where: { productId: product.id },
    });
    const localNames = await extra.productLocalNames;
    const foundName = localNames.find(
      (localName: ProductName) => localName.lang === lang
    )?.value;
    return normalize(foundName || localNames[0].value);
  }

  @FieldResolver((type) => String, { nullable: true })
  async description(
    @Root() product: Product,
    @Ctx() { prisma, extra }: Context,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<string | null> {
    const descriptions = await prisma.description.findMany({
      where: { productId: product.id },
    });

    const foundDescription = descriptions.find(
      (description: Description) => description.lang === lang
    )?.value;
    return foundDescription || descriptions[0]?.value;
  }
}

@Resolver((of) => Category)
export class CustomCategoryResolver {
  @FieldResolver((type) => String, { nullable: true })
  async name(
    @Root() category: Category,
    @Ctx() { prisma, extra }: Context,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<string> {
    extra.categoryLocalNames ||= prisma.categoryName.findMany({
      where: { categoryId: category.id },
    });
    const localNames = await extra.categoryLocalNames;
    const foundName = localNames.find(
      (localName: CategoryName) => localName.lang === lang
    )?.value;
    return foundName || localNames[0].value;
  }

  @FieldResolver((type) => String, { nullable: true })
  async url_safe_name(
    @Root() category: Category,
    @Ctx() { prisma, extra }: Context,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<string> {
    extra.categoryLocalNames ||= prisma.categoryName.findMany({
      where: { categoryId: category.id },
    });
    const localNames = await extra.categoryLocalNames;
    const foundName = localNames.find(
      (localName: CategoryName) => localName.lang === lang
    )?.value;
    return normalize(foundName || localNames[0].value);
  }

  @FieldResolver((type) => String, { nullable: true })
  async categoryTreeNames(
    @Root() category: Category,
    @Ctx() { prisma, extra }: Context,
    @Arg("lang", () => Language, { nullable: true, defaultValue: Language.hu })
    lang: Language
  ): Promise<string | null> {
    extra.categoryLocalNames ||= prisma.categoryName.findMany({
      where: { categoryId: category.id },
    });
    const localNames = await extra.categoryLocalNames;
    const foundName = localNames.find(
      (localName: CategoryName) => localName.lang === lang
    )?.categoryTreeNames;
    return foundName || localNames[0].categoryTreeNames;
  }
}

registerEnumType(Language, {
  name: "Language_enum",
});

applyResolversEnhanceMap(resolversEnhanceMap);
CacheMiddleware.applyResolversEnhanceMap();
ElasticMiddleware.applyResolversEnhanceMap();
CategoryTreeBuilder.applyResolversEnhanceMap();

async function main() {
  const prisma = new PrismaClient();
  @Module({
    imports: [
      CacheModule.register(),
      TypeGraphQLModule.forRoot({
        playground: process.env.PROD === "false",
        introspection: true,
        emitSchemaFile: path.resolve(__dirname, "./generated-schema.graphql"),
        validate: false,
        plugins: [responseCachePlugin()],
        cacheControl: {
          calculateHttpHeaders: true,
        },
        cors: {
          origin: [
            "http://localhost:4200",
            "http://localhost:3000",
            "localhost:4200",
            "localhost:3000",
          ],
          methods: ["GET", "PUT", "POST"],
          credentials: true,
        },
        authChecker: customAuthChecker,
        context: ({ req, res, extra }): Context => ({
          prisma,
          req,
          res,
          extra: {},
        }),
      }),
    ],
    providers: [
      ...(Array.from(crudResolvers.values()) as Provider[]),
      ...(Array.from(relationResolvers.values()) as Provider[]),
      CategoryTreeBuilder,
      SearchResolver,
      CustomProductResolver,
      CustomCategoryResolver,
    ],
    controllers: [AppController],
  })
  class AppModule {}

  const app = await NestFactory.create(AppModule, new ExpressAdapter());
  app.enableCors({
    origin: [
      "http://localhost:4200",
      "http://localhost:3000",
      "localhost:3000",
      "localhost:4200",
    ],
    credentials: true,
  });
  app.use(cookieParser());
  await app.listen(3000);
  console.log(`GraphQL is listening on 3000!`);
}

main().catch(console.error);
